<?php

/**
 * @file taxonomy_grid.admin.inc
 * Functions that are only called on the admin pages.
 */

/**
 * Module settings form.
 */
function taxonomy_grid_settings() {

  $form['taxonomy_grid_info'] = array(
    '#value' => t('<p>The taxonomy_grid module provides a page containing content types and associated terms</p>')
      .  t('<p>Together with the taxonomy_image.module, the list can be displayed with a image icon.</p>')
    );
	
    
  // General settings.
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    ); 
    	
	$form['general']['taxonomy_grid_cells_per_row'] = array(
	   	'#title' => t('Number of cells per row'),
	   	'#type' => 'textfield',
	   	'#default_value' => variable_get('taxonomy_grid_cells_per_row', 3),
	   	'#options' => $select,	
		'#size' => 2,
		'#maxlength' => 2,
	  	'#description' => t('Set how many cells per row you want.'),
    	'#required' => TRUE,
	);
	

    $form['general']['taxonomy_grid_render_style'] = array(
      '#type' => 'select',
      '#title' => t('Grid display style'),
      '#default_value' => variable_get('taxonomy_grid_render_style','div'),
      '#options' => array(
        'div' => t('Display the grid using divs'),
        'table'    => t('Display the grid using a table'),
      ),
    );
    		
  $form['general']['taxonomy_grid_show_addlink'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add a link to add a content?'),
    '#default_value' => variable_get('taxonomy_grid_show_addlink', TRUE),
    '#description' => t('Should we add a link to add a content in the corresponding displayed block?'),
    );
    
   $form['general']['taxonomy_grid_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page title'),
    '#default_value' => variable_get('taxonomy_grid_title', "Catalogue"),
    '#description' => t('Title that be displayed on the page'),
    );
    
   $form['general']['taxonomy_grid_show_images'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show images?'),
    '#default_value' => variable_get('taxonomy_grid_show_images', true),
    '#description' => t('Do we show images (need Taxonomy Image module)?'),
    );
  // Meta settings.
  $form['meta'] = array(
    '#type' => 'fieldset',
    '#title' => t('Meta settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    ); 
    	
	$form['meta']['taxonomy_grid_keywords'] = array(
	   	'#title' => t('Keywords'),
	   	'#type' => 'textarea',
	    '#cols' => 60,
    	'#rows' => 5,
		'#maxlength' => 1000,
	   	'#default_value' => variable_get('taxonomy_grid_keywords',""),
	  	'#description' => t('Set the meta keywords.'),
	   	'#required' => FALSE,
	);   
	$form['meta']['taxonomy_grid_description'] = array(
	   	'#title' => t('Description'),
	   	'#type' => 'textfield',
		'#maxlength' => 200,
		'#default_value' => variable_get('taxonomy_grid_description',""),
	  	'#description' => t('Set the meta description.'),
	   	'#required' => FALSE,
	); 
    
   // Vocabularies settings.
  $form['vocabularies'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vocabularies'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );  
     
  $select = array();
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vocabulary) {
    $select[$vocabulary->vid] = $vocabulary->name.'<br /><em>'.$vocabulary->description."</em>";
  }
  $form['vocabularies']['taxonomy_grid_vocabularies'] = array(
    '#title' => t('Included Vocabularies'),
    '#type' => 'checkboxes',
    '#default_value' => variable_get('taxonomy_grid_vocabularies', array()),
    '#options' => $select,
    '#description' => t('Select the vocabularies the user can select from on the category browser page.'),
    '#prefix' => '<div class="taxonomy_grid_checkboxes">',
    '#suffix' => '</div>',
    );
    
  // Types settings.
  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $select = array();
  $types = node_get_types();
  $names = node_get_types('names');
  foreach ($types as $key => $type) {
    $select[$key] = $names[$key];
	
  } 
  $form['types']['taxonomy_grid_types'] = array(
    '#title' => t('Included Types'),
    '#type' => 'checkboxes',
    '#default_value' => variable_get('taxonomy_grid_types', array()),
    '#options' => $select,
    '#description' => t('Select the types the user can select from on the category browser page.'),
    '#prefix' => '<div class="taxonomy_grid_checkboxes">',
    '#suffix' => '</div>',
    );  
        
  return system_settings_form($form);
}